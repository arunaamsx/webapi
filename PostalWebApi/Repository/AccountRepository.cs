﻿using PostalWebApi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace PostalWebApi.Repository
{
    public class AccountRepository
    {

        IAccountService authenticationService;

        public AccountRepository(IAccountService service)
        {
            authenticationService = service;
        }

        public  HttpResponseMessage GetAccountDetails(string phoneNumber, string userName)
        {
            return authenticationService.GetAccountDetails(phoneNumber, userName);
        }

        public HttpResponseMessage GetTrxDetails(string phoneNumber, string userName)
        {
            return authenticationService.GetTrxDetails(phoneNumber, userName);
        }
    }


}