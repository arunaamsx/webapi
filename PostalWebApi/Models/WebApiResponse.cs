﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;

namespace PostalWebApi.Models
{
    public class WebApiResponse : HttpResponseMessage
    {
        private MessageContent messageContent = null;

        public WebApiResponse()
            : base()
        {
            base.StatusCode = HttpStatusCode.OK;

            this.messageContent = new MessageContent();
         

            this.SetContent();
        }

        public WebApiResponse(HttpStatusCode statusCode)
            : this()
        {
            this.StatusCode = statusCode;
        }

       


        protected virtual void SetContent()
        {
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings()
            {
                Formatting = Formatting.None,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            string serializedObj = JsonConvert.SerializeObject(this.messageContent, serializerSettings);
            base.Content = new StringContent(serializedObj, Encoding.UTF8, "application/json");
        }

      
    }
}