﻿using PostalWebApi.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostalWebApi.Models
{
    public class DataConverter
    {

        public AccountInfoViewModel AccountDataConvert(string accountjson)
        {
            AccountInfoViewModel objaccountmodel = new AccountInfoViewModel();
            try
            {
                if (!string.IsNullOrEmpty(accountjson))
                {
                    List<string> list = accountjson.Split('|').ToList();
                    objaccountmodel.BranchCode = list[0];
                    objaccountmodel.BranchSubCode = list[1];
                    objaccountmodel.BarnchName = list[2];
                    objaccountmodel.DateOfBirth = list[3];
                    objaccountmodel.AccountStatus = list[4];
                    objaccountmodel.AccountCatagory = list[5];
                    objaccountmodel.AccountOpenDate = list[6];
                    objaccountmodel.YearBeginBal = list[7];
                    objaccountmodel.CurBalance = list[8] + list[9];
                    objaccountmodel.LastTrxDate = list[10];
                    objaccountmodel.LastActive = list[11];
                    objaccountmodel.AccountHolderInfo = new List<UserInfoModel>()
                                                          {
                              new UserInfoModel() {
                                  AccountHolderLastName = list[13] ,
                                  AccountHolderName =list[14] ,
                                  Nic = list[15] } };             
                }
            }
            catch (Exception)
            {

                throw;
            }
            return objaccountmodel;
        }

        internal List<TrxInfoViewModel> TrxDataConvert(string responseDetail)
        {
            List<TrxInfoViewModel> txninfoviewlist = new List<TrxInfoViewModel>();
            TrxInfoViewModel objtrxInfoViewModel;
            try
            {
                if (!string.IsNullOrEmpty(responseDetail))
                {
                    List<string> list = responseDetail.Split('|').ToList();

                    foreach (var item in list)
                    {
                        List<string> txndata = item.Split('!').ToList();
                        objtrxInfoViewModel = new TrxInfoViewModel();
                        objtrxInfoViewModel.BatchNo = txndata[4];
                        objtrxInfoViewModel.Date = txndata[0];
                        objtrxInfoViewModel.Deposit = txndata[2];
                        objtrxInfoViewModel.Description = txndata[1];
                        objtrxInfoViewModel.PBPrint = txndata[5];
                        objtrxInfoViewModel.Withdrawal= txndata[3];
                        objtrxInfoViewModel.colur =  !string.IsNullOrEmpty(objtrxInfoViewModel.Deposit) ? "#FF2F2F" : "#5DFF43";
                        objtrxInfoViewModel.Amount = string.IsNullOrEmpty(objtrxInfoViewModel.Deposit) ? objtrxInfoViewModel.Withdrawal : objtrxInfoViewModel.Deposit;


                        txninfoviewlist.Add(objtrxInfoViewModel);
                    }
                   
                }
            }
            catch (Exception)
            {

                throw;
            }
            return txninfoviewlist.OrderByDescending(c=>c.Date).ToList();
        }
       
    }
}