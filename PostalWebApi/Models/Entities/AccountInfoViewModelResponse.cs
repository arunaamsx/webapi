﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Web;

namespace PostalWebApi.Models.Entities
{
    [CollectionDataContract]
    public class AccountInfoViewModelResponse 
    {
        [DataMember]
        public AccountInfoViewModel AccountinfoView { get; set; }
    }
}