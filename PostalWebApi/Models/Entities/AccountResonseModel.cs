﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostalWebApi.Models.Entities
{
    public class AccountResonseModel
    {

        [JsonProperty("ResponseCode")]
        public string ResponseCode { get; set; }
        [JsonProperty("ResponseDescription")]
        public string ResponseDescription { get; set; }
        [JsonProperty("ResponseDetail")]
        public string ResponseDetail { get; set; }
      
    }
}