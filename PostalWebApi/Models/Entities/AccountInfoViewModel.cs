﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostalWebApi.Models.Entities
{
    public class AccountInfoViewModel
    {

        public string AccountNumber { get; set; }

        public IEnumerable<UserInfoModel> AccountHolderInfo { get; set; }

        public string BranchCode { get; set; }

        public string BranchSubCode { get; set; }

        public string BarnchName { get; set; }

        public string DateOfBirth { get; set; }

        public string AccountStatus { get; set; }

        public string AccountCatagory { get; set; }

        public string AccountOpenDate { get; set; }

        public string YearBeginBal { get; set; }

        public string CurBalance { get; set; }

        public string LastTrxDate { get; set; }

        public string LastActive { get; set; }



    }

    public class UserInfoModel
    {

        public string AccountHolderName { get; set; }

        public string AccountHolderLastName { get; set; }

        public string Nic { get; set; }

    }


    public class TrxInfoViewModel
    {

        public string Date { get; set; }

        public string Description { get; set; }

        public string Deposit { get; set; }

        public string Withdrawal { get; set; }

        public string BatchNo { get; set; }

        public string PBPrint { get; set; }

        public string colur { get; set; }

        public string Amount { get; set; }
    }

    public class TrxInfoViewListModel
    {

        public List<TrxInfoViewModel> TxnDataList { get; set; }

       
    }
}




