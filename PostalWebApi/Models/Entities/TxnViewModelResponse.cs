﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PostalWebApi.Models.Entities
{
    [CollectionDataContract]
    public class TxnViewModelResponse
    {
        [DataMember]
        public List<TrxInfoViewModel> TrxinfoView { get; set; }
    }
}