﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PostalWebApi.Models.Identity
{
    public class AccountBindingModel
    {

        [Required]
        public string AccNumber { get; set; }


        [Required]
        public string UserName { get; set; }
    }
}