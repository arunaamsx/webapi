﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PostalWebApi.Models;
using PostalWebApi.Models.Entities;
using PostalWebApi.Models.Identity;
using PostalWebApi.Repository;
using PostalWebApi.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PostalWebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    //[Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        AccountRepository autheticationRepo = new AccountRepository(new AccountService());

        // POST api/Account
        [Route("GetAccountInfo")]
        [HttpPost]
        public AccountInfoViewModelResponse GetAccountInfo(AccountBindingModel model)
        {

            AccountResonseModel responseValue = new AccountResonseModel();
            AccountInfoViewModelResponse response = new AccountInfoViewModelResponse();
            if (!ModelState.IsValid)
            {
                return response;
            }

            else
            {
                responseValue.ResponseCode = "1";
                responseValue.ResponseDescription = "";
                responseValue.ResponseDetail = Constant.Accoung(model.AccNumber);
                if (responseValue.ResponseCode == "1")
                {
                    AccountInfoViewModel info = new DataConverter().AccountDataConvert(responseValue.ResponseDetail);
                    response.AccountinfoView = info;
                }
                else
                {
                    response.AccountinfoView = null;

                }
            }
            return response;
        }



        // POST api/GetTrxInfo
        [Route("GetTrxInfo")]
        [HttpPost]
        public TxnViewModelResponse GetTrxInfo(AccountBindingModel model)
        {
            TxnResponseModel responseValue = new TxnResponseModel();
            TxnViewModelResponse response = new TxnViewModelResponse();
            if (!ModelState.IsValid)
            {
                return response;
            }

            else
            {

                responseValue.ResponseCode = "1";
                responseValue.ResponseDescription = "";
                responseValue.ResponseDetail = Constant.Txn(model.AccNumber);
                if (responseValue.ResponseCode == "1")
                {
                    List<TrxInfoViewModel> info = new DataConverter().TrxDataConvert(responseValue.ResponseDetail);
                    response.TrxinfoView = info;
                }
                else
                {
                    response.TrxinfoView = null;
                }

            }





            return response;

        }

        [Route("GetTest")]
        [HttpGet]
        public string GetTest()
        {

            return "Aruna";

        }
    }
}