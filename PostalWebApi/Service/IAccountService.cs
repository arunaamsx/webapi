﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace PostalWebApi.Service
{
    public interface IAccountService
    {
       HttpResponseMessage GetAccountDetails(string AccNumber, string userName);

        HttpResponseMessage GetTrxDetails(string AccNumber, string userName);
    }
}