﻿using Newtonsoft.Json;
using PostalWebApi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PostalWebApi.Service
{
    public class AccountService : IAccountService
    {

        private readonly Uri baseurl = new Uri(ConfigurationManager.AppSettings["PostalUrl"]);
        public HttpResponseMessage GetAccountDetails(string accountnumber, string userName)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            try
            {

                var response = string.Empty;
                using (var client = new HttpClient())
                {

                    var uri = new Uri(baseurl, string.Format("api/Account/GetTest"));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                     result =  client.GetAsync(uri).Result;

                }

            }
            catch (Exception ex)
            {

               
            }


            return result;

        }

        public HttpResponseMessage GetTrxDetails(string AccNumber, string userName)
        {
            HttpResponseMessage result = new HttpResponseMessage();
            try
            {

                var response = string.Empty;
                using (var client = new HttpClient())
                {

                    var uri = new Uri(baseurl, string.Format("transaction/{0}/{1}", AccNumber, userName));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    result = client.GetAsync(uri).Result;

                }

            }
            catch (Exception ex)
            {


            }


            return result;
        }
    }
}